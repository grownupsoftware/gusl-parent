#!/bin/sh

echo "Parent"
git status
for i in $(cat modules)
do
	if [ -d $i ]; then
		echo "Status " $i
		cd $i
		git status | grep -v "On branch master" | grep -v "Your branch is up to date with 'origin/master'" | grep -v "nothing to commit, working tree clean"
		cd ..
	fi
done
