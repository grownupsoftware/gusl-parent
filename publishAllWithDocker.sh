#!/bin/sh
set -e
for i in `cat modules`
do
	if [ -d "$i" ]; then
		echo "Compiling " $i
		cd "$i"
		git pull
		cd ..
    ./gradlew "$i":clean "$i":compileJava "$i":publish
	fi
done
  ./gradlew gusl-launcher:buildImage gusl-launcher:pushImage

