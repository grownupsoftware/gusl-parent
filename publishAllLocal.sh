#!/bin/sh
set -e
for i in `cat modules`
do
	if [ -d "$i" ]; then
		echo "Compiling " $i
    ./gradlew "$i":clean "$i":compileJava "$i":publishToMavenLocal
	fi
done
