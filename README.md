![Alt text](https://www.googleapis.com/download/storage/v1/b/gusl-images/o/logo.png?generation=1588764693002370&alt=media)

# gusl-parent

# Getting Started

gusl-parent is a container gradle project which owns the gradle.properties for the sub projects

## Getting Started
The following repos should be cloned locally using ./cloneAll *BitBucketUser*

There is a file of Modules, add to this to include the modules in all of the scripts.


```shell script
./updateAll
```

### Publishing

```shell
gcloud auth application-default login
```

````shell
gcloud artifacts print-settings gradle --project=gusl-common --repository=gusl-common --location=europe-west2
````
# gusl-idea

# Getting Started

on the plugins page (Settings), Press the cog and install plugin from disk.
